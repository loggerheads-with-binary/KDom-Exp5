A=importdata('Data.dat');     
D=A.data;

%Data Visualization
Time=D(1:2:end,1);                    
a0=D(1:2:end,4);                     
dt=(Time(2)-Time(1));
Fs=1/dt;                        
a0 = a0 - mean(a0)

figure (1)                      
plot(Time,a0)
ylabel('Acceleration (m/s^2)')
xlabel('Time, t (sec)')

x0_filtered=smooth(a0);
figure
plot(x0_filtered)

signal=x0_filtered;                 
L=length(signal);


X1 = fft(signal)/(L/2);
X1 = X1(1:L/2);
mx1 = abs(X1);                      
f = linspace(0,Fs/2,L/2)

figure                         
plot(f*(2*pi),(mx1),'LineWidth',2);               
xlabel('Frequency')
ylabel('|Amplitude|')
xlim([0 300])