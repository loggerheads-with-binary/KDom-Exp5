A = 10;
Omega = 10; 
f = Omega/(2*pi);                    
Fs = 10 ; 
dt = 1/Fs; 
t_end = 1.5*pi/Omega
Time = 0:dt:t_end                      

F = A*sin(Omega*Time)   

plot(Time,F,'-','Marker','.','MarkerSize',20,'LineWidth',2)
axis([0 Time(end) -1.5*A 1.5*A])
xlabel('Time')
ylabel('F= A sin(omega*t)')
title("Signal with Fs= " + string(Fs))
xlim([0 1])
grid on

L=length(F);
w  = hann(L) ; 
plot(w,'LineWidth',2)

x1=F.*w';
figure
plot(x1)
title("effect of Hann on signal")

X1 = fft(x1)/(L/2);
X1 = X1(1:L/2);
mx1 = abs(X1);                      
f = linspace(0,Fs/2,L/2)

figure                          % Plotting
plot(f*(2*pi),(mx1),'LineWidth',2);               % Convert Hz to radians
xlabel('Frequency')
ylabel('|Amplitude|')
title("FFT")
xlim([0 100])

Fnoise = F + 3*randn(size(F));      plot(Time,Fnoise,'LineWidth',2)
Ffiltered = smooth(Fnoise);         %Filtering:moving average (low pass)
hold on
plot(Time,Ffiltered,'r','LineWidth',2)

x1=Fnoise.*w';
figure
plot(x1)

X1 = fft(x1)/(L/2);
X1 = X1(1:L/2);
mx1 = abs(X1);                     

f = linspace(0,Fs/2,L/2)

figure                          
plot(f*(2*pi),(mx1),'LineWidth',2);               % Convert Hz to radians
xlabel('Frequency')
ylabel('|Amplitude|')
xlim([0 500])